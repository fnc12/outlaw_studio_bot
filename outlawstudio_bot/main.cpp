//
//  main.cpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#include <iostream>
#include <utility>
#include <sqlite_orm/sqlite_orm.h>
#include <vector>
#include <atomic>
#include <thread>

#include "Services/TelegramAPI.hpp"
#include "Services/LockServer.hpp"

using Services::TelegramAPI;
using Services::LockServer;

using std::cout;
using std::cerr;
using std::endl;

using DataModel::Update;

typedef std::pair<std::string, std::string> KeyValue;

struct AllowedUsers {
    std::string username;
};

static std::atomic_int shouldOpen;
static const std::string lastUpdateIdKey = "lastUpdateId";

using namespace sqlite_orm;
auto storage = make_storage("db.sqlite",
                            make_table("key_value",
                                       make_column("key",
                                                   &KeyValue::first),
                                       make_column("value",
                                                   &KeyValue::second)),
                            make_table("allowed_users",
                                       make_column("username",
                                                   &AllowedUsers::username)));

bool processUpdates(const std::vector<Update> &updates) {
    auto res = false;
    for(auto &update : updates) {
        if(update.message) {
            auto &message = *update.message;
            if(message.from){
                auto &from = *message.from;
                if(from.username){
                    auto &username = *from.username;
//                    cout << "username = " << username << endl;
                    auto rows = storage.get_all<AllowedUsers>(where(is_equal(&AllowedUsers::username, username)));
                    if(rows.size()){
                        res = true;
                        cout << "opened to " << username << endl;
                    }else{
                        cout << "not opened to " << username << endl;
                    }
                }else{
                    cerr << "from.username is null with user id " << from.id << endl;
                }
            }else{
                cerr << "message.from is null with message_id " << message.message_id << endl;
            }
        }else{
            cerr << "update.message is null with update_id " << update.update_id << endl;
        }
    }
    return res;
}

void setLastUpdateId(long updateId) {
    storage.transaction([&] {
        storage.remove_all<KeyValue>(where(is_equal(&KeyValue::first, lastUpdateIdKey)));
        storage.insert(KeyValue{lastUpdateIdKey, std::to_string(updateId)});
        return true;
    });
}

long getLastUpdateId() {
    auto rows = storage.get_all<KeyValue>(where(is_equal(&KeyValue::first, lastUpdateIdKey)));
    if(rows.size()){
        return std::stol(rows.front().second);
    }else{
        return 0;
    }
}

int main(int argc, const char * argv[]) {
    
    cout << "path = " << argv[0] << endl;
    
    storage.sync_schema();
    
    LockServer lockServer;
    lockServer.getOpened = [&] {
        return shouldOpen.load();
    };
    std::thread lockServerThread([&] {
        lockServer.run();
    });
    
    auto tgApiKey = argv[1];
    cout << "tgApiKey = " << tgApiKey << endl;
    
    TelegramAPI telegramAPI(tgApiKey);
    auto bot = telegramAPI.getMe();
    cout << "bot = " << bot << endl;
    
    while(true){
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
        auto offset = getLastUpdateId() + 1;
//        auto offset = 14033023;
        auto limit = 10;
        auto updates = telegramAPI.getUpdates(offset, limit);
        if(updates.size()){
            auto shouldOpenNow = processUpdates(updates);
            shouldOpen.store(shouldOpenNow);
            cout << "shouldOpen = " << shouldOpen.load() << endl;
            setLastUpdateId(updates.back().update_id);
        }else{
            shouldOpen.store(false);
            cout << "waiting.." << endl;
        }
    }
    
    return 0;
}
