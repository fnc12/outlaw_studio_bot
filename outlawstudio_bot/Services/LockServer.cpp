//
//  LockServer.cpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 22.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#include "LockServer.hpp"

#include <csignal>
#include <iostream>

using std::cout;
using std::endl;

Services::LockServer::LockServer() {
#ifndef __APPLE__
    {
        // ignore SIGPIPE in linux..
        struct sigaction act;
        act.sa_handler=SIG_IGN;
        sigemptyset(&act.sa_mask);
        act.sa_flags=0;
        sigaction(SIGPIPE, &act, NULL);
        /*err(1, "sigaction");*/
    }
#endif
}

int Services::LockServer::run() {
    this->socket.bindToPort(kPort);
    this->socket.listenForConnections(kListenBacklog);
    while(true) {
        auto clientSocket = this->socket.acceptConnection();
        auto message = this->closeMessage;
        if(getOpened){
            auto opened = getOpened();
            if(opened){
                message = this->openMessage;
            }
        }
        cout << "message = " << message << endl;
        clientSocket.writeBytes(message.c_str(), message.length());
        clientSocket.close();
    }
}
