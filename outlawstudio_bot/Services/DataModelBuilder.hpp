//
//  DataModelBuilder.hpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#ifndef DataModelBuilder_hpp
#define DataModelBuilder_hpp

#include "DataModel/User.hpp"
#include "DataModel/Chat.hpp"
#include "DataModel/Message.hpp"
#include "DataModel/Update.hpp"

#include <json/json.hpp>

namespace Services {
    
    struct DataModelBuilder {
        
    };
}

namespace DataModel {
    
    using nlohmann::json;
    
    void to_json(json& j, const User& user);
    
    void from_json(const json& j, User& user);
    
    void to_json(json& j, const Chat& bot);
    
    void from_json(const json& j, Chat& bot);
    
    void to_json(json& j, const Message& bot);
    
    void from_json(const json& j, Message& bot);
    
    void to_json(json& j, const Update& obj);
    
    void from_json(const json& j, Update& obj);
}

#endif /* DataModelBuilder_hpp */
