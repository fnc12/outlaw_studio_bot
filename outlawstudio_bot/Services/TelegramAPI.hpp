//
//  TelegramAPI.hpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#ifndef TelegramAPI_hpp
#define TelegramAPI_hpp

#include <string>
#include <vector>

#include "DataModel/User.hpp"
#include "DataModel/Update.hpp"

namespace Services {
    
    using DataModel::User;
    using DataModel::Update;
    
    struct TelegramAPI {
        
        TelegramAPI(const std::string &token_):token(token_){}
        
        User getMe();
        
        std::vector<Update> getUpdates(long offset, long limit);
        
    protected:
        
        std::string token;
        
        std::string createUrl(const std::string &postfix) const;
    };
}

#endif /* TelegramAPI_hpp */
