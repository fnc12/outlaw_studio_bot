//
//  TelegramAPI.cpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#include "TelegramAPI.hpp"
#include "Services/DataModelBuilder.hpp"

#include <curl/curl.h>
#include <sstream>
#include <iostream>
#include <json/json.hpp>
#include <stdexcept>

using std::cout;
using std::endl;

using DataModel::User;
using DataModel::Update;

using DataModel::from_json;
using DataModel::to_json;

using json = nlohmann::json;

void function_pt(void *ptr, size_t size, size_t nmemb, void *stream){
//    printf("%d", atoi(ptr));
    auto &ss = *(std::stringstream*)stream;
    ss << std::string((const char*)ptr, size * nmemb);
}

User Services::TelegramAPI::getMe() {
    if(auto curl = curl_easy_init()) {
        auto url = createUrl("getMe");
        cout << "url = " << url << endl;
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        std::stringstream ss;
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, function_pt);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ss);
        curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        auto responseData = ss.str();
        auto j = json::parse(responseData);
        return j["result"];
    }else{
        throw std::runtime_error("error");
    }
}

std::vector<Update> Services::TelegramAPI::getUpdates(long offset, long limit) {
    if(auto curl = curl_easy_init()) {
        auto url = createUrl("getUpdates?offset=" + std::to_string(offset) + "?limit=" + std::to_string(limit));
        cout << "url = " << url << endl;
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        std::stringstream ss;
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, function_pt);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ss);
        curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        auto responseData = ss.str();
//        cout << "responseData = " << responseData << endl;
        auto j = json::parse(responseData);
//        j["result"].get<Bot>();
        return j["result"];
    }else{
        throw std::runtime_error("error");
    }
}

std::string Services::TelegramAPI::createUrl(const std::string &postfix) const {
    return "https://api.telegram.org/bot" + this->token + "/" + postfix;
}
