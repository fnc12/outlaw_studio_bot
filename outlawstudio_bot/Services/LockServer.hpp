//
//  LockServer.hpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 22.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#ifndef LockServer_hpp
#define LockServer_hpp

#include <TCPSocket/TCPSocket.h>
#include <functional>
#include <string>

namespace Services {
    
    struct LockServer {
        
        LockServer();
        
        int run();
        
        enum {
            kPort = 14975,
            kListenBacklog = 1024,
        };
        
        std::function<bool()> getOpened;
    protected:
        TCPSocket socket;
        const std::string openMessage = "opn";
        const std::string closeMessage = "cls";
//        const uint32_t openMessage = 200;
//        const uint32_t closeMessage = 0;
    };
}

#endif /* LockServer_hpp */
