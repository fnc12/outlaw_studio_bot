//
//  DataModelBuilder.cpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#include "DataModelBuilder.hpp"

#include <string>

#define KEY(x) const std::string x = #x

namespace userKeys {
    KEY(id);
    KEY(first_name);
    KEY(last_name);
    KEY(username);
}

namespace chatKeys {
    KEY(id);
    KEY(first_name);
    KEY(last_name);
    KEY(username);
    KEY(type);
};

namespace messageKeys {
    KEY(message_id);
    KEY(from);
    KEY(chat);
    KEY(date);
    KEY(text);
}

namespace updateKeys {
    KEY(update_id);
    KEY(message);
}

void DataModel::to_json(json& j, const User& user) {
//    auto &k = userKeys;
    using namespace userKeys;
    j = {
        { id, user.id },
        { first_name, user.first_name },
//        { username, user.username },
    };
    if(user.username){
        j[username] = *user.username;
    }
    if(user.last_name){
        j[last_name] = *user.last_name;
    }
}

void DataModel::from_json(const json& j, User& user) {
//    auto &k = userKeys;
    using namespace userKeys;
    user.id = j[id];
    user.first_name = j[first_name];
//    user.username = j[username];
    auto it = j.find(username);
    if(it != j.end()){
        user.username = std::make_shared<std::string>(it->get<std::string>());
    }
    it = j.find(last_name);
    if(it != j.end()){
        user.last_name = std::make_shared<std::string>(it->get<std::string>());
    }
}

void DataModel::to_json(json& j, const Chat& o) {
    using namespace chatKeys;
    j = {
        { id, o.id },
        { first_name, o.first_name },
        { last_name, o.last_name },
//        { username, bot.username },
        { type, o.type },
    };
    if(o.username){
        j[username] = *o.username;
    }
}

void DataModel::from_json(const json& j, Chat& o) {
    using namespace chatKeys;
    o.id = j[id];
    o.first_name = j[first_name];
    o.last_name = j[last_name];
    auto it = j.find(username);
    if(it != j.end()){
        o.username = std::make_shared<std::string>(it->get<std::string>());
    }
    o.type = j[type];
}

void DataModel::to_json(json& j, const Message& message) {
    using namespace messageKeys;
    j = {
        { message_id, message.message_id },
//        { from, message.from },
        { chat, message.chat },
        { date, message.date },
//        { text, message.text },
    };
    if(message.from){
        j[from] = *message.from;
    }
    if(message.text){
        j[text] = *message.text;
    }
}

void DataModel::from_json(const json& j, Message& message) {
    using namespace messageKeys;
    message.message_id = j[message_id];
//    message.from = j[from];
    auto it = j.find(from);
    if(it != j.end()){
        message.from = std::make_shared<User>(*it);
    }
    it = j.find(text);
    if(it != j.end()){
        message.text = std::make_shared<std::string>(it->get<std::string>());
    }
    message.chat = j[chat];
    message.date = j[date];
//    message.text = j[text];
}

void DataModel::to_json(json& j, const Update& obj) {
    using namespace updateKeys;
    j = {
        { update_id, obj.update_id },
//        { message, obj.message },
    };
    if(obj.message) {
        j[message] = *obj.message;
    }
}

void DataModel::from_json(const json& j, Update& obj) {
    using namespace updateKeys;
    obj.update_id = j[update_id];
    auto it = j.find(message);
    if(it != j.end()) {
        obj.message = std::make_shared<Message>(*it);
    }
}

#undef KEY
