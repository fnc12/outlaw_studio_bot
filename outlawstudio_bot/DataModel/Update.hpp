//
//  Update.hpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#ifndef Update_hpp
#define Update_hpp

#include <json/json.hpp>
#include <memory>

#include "Message.hpp"

namespace DataModel {
    
    struct Update {
        long update_id;
        std::shared_ptr<Message> message;
    };
    
    using nlohmann::json;
}

#endif /* Update_hpp */
