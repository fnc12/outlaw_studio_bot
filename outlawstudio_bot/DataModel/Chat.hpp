//
//  Chat.hpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#ifndef Chat_hpp
#define Chat_hpp

#include <string>
#include <json/json.hpp>
#include <memory>

namespace DataModel {
    
    struct Chat {
        long id;
        std::string first_name;
        std::string last_name;
        std::shared_ptr<std::string> username;
        std::string type;
    };
    
    using nlohmann::json;
}

#endif /* Chat_hpp */
