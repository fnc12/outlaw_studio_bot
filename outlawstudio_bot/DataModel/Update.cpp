//
//  Update.cpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#include "Update.hpp"

#define KEY(x) static const std::string x = #x

#undef KEY
