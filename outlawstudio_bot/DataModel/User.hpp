//
//  Bot.hpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#ifndef Bot_hpp
#define Bot_hpp

#include <string>
#include <ostream>
#include <memory>

namespace DataModel {
    
    struct User {
        long id;
        std::string first_name;
        std::shared_ptr<std::string> last_name;
        std::shared_ptr<std::string> username;
    };
    
    std::ostream& operator<<(std::ostream &os, const User &user);
}

#endif /* Bot_hpp */
