//
//  Bot.cpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#include "User.hpp"

std::ostream& DataModel::operator<<(std::ostream &os, const User &user) {
    
    return os << "[ id : " << user.id << ", first_name : " << user.first_name << " ]";
}
