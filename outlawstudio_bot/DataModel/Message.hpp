//
//  Message.hpp
//  outlawstudio_bot
//
//  Created by John Zakharov on 20.02.17.
//  Copyright © 2017 Outlaw Studio. All rights reserved.
//

#ifndef Message_hpp
#define Message_hpp

#include <sys/time.h>
#include <string>
#include <json/json.hpp>
#include <memory>

#include "User.hpp"
#include "Chat.hpp"

namespace DataModel {
    
    struct Message {
        long message_id;
        std::shared_ptr<User> from;
        Chat chat;
        time_t date;
        std::shared_ptr<std::string> text;
    };
    
    using nlohmann::json;
}

#endif /* Message_hpp */
